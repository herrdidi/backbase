package ComputersDataBase;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.WebDriver;
import utils.CustomBrowser;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BaseTest {

    protected WebDriver driver;
    protected String timeStamp;

    @BeforeEach
    public void setupTestCase(){
        CustomBrowser browser = new CustomBrowser();
        browser.startBrowser();
        driver = browser.getDriver();
        timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
    }

    @AfterEach
    public void closeTest(){
        driver.quit();
    }
}
