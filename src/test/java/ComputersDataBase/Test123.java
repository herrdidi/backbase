package ComputersDataBase;

import org.junit.jupiter.api.Test;
import pages.AddNewComputer;
import pages.Header;
import pages.HomePage;
import pages.EditComputer;

public class Test123 extends BaseTest {

    @Test
    public void addComputerDetails(){
        HomePage homePage = new HomePage(driver);
        AddNewComputer computer = new AddNewComputer(driver);
        String computerName = "CN-" + timeStamp;

        homePage.addButton.click();

        computer.addComputerDetails(computerName,"1990-02-17","2019-02-14","IBM");
        homePage.verifyComputerCreadedMessage(computerName);
        homePage.searchForComputer(computerName);
        homePage.verifyValuesForRowAre(1,computerName,"17 Feb 1990","14 Feb 2019","IBM");
    }

    @Test
    public void addComputerPageValidations(){
        HomePage homePage = new HomePage(driver);
        AddNewComputer computerPage = new AddNewComputer(driver);
        String computerName = "CN-" + timeStamp;

        homePage.addButton.click();
        computerPage.introducedDateInput.sendKeys("meh");
        computerPage.discontinuedDateInput.sendKeys("meh");
        computerPage.submitBtn.click();
        computerPage.verifyErrorFor("Computer name");
        computerPage.verifyErrorFor("Introduced date");
        computerPage.verifyErrorFor("Discontinued date");
        computerPage.addComputerDetails(computerName);
        homePage.searchForComputer(computerName);
        homePage.verifyComputerIsPresent(computerName);
    }

    @Test
    public void homePage(){
        HomePage homePage = new HomePage(driver);

        homePage.searchForComputer("z1111");
        homePage.verifyValueUnderRowForColumnIs(1,"Computer name", "z1111");
        homePage.verifyTextForHeaderIs("One computer found");
        homePage.verifyNextButtonIsDisabled();
        homePage.verifyPrevButtonIsDisabled();
        homePage.verifyTextForPaginationSearchResultsIs("Displaying 1 to 1 of 1");
    }

    @Test
    public void homePageElements(){
        HomePage homePage = new HomePage(driver);
        Header header = new Header(driver);

        header.verifyTopBarText();
        homePage.verifyPrevButtonIsDisabled();
        homePage.verifyNextButtonIsEnabled();
        homePage.goToNextPage();
        homePage.verifyPrevButtonIsEnabled();
        homePage.verifyNextButtonIsEnabled();
        header.headerLink.click();
        homePage.verifyPrevButtonIsDisabled();
        homePage.verifyNextButtonIsEnabled();
    }

    @Test
    public void deleteComputer(){
        HomePage homePage = new HomePage(driver);
        EditComputer editComputer = new EditComputer(driver);

        homePage.searchForComputer("z5555");
        homePage.verifyComputerIsPresent("z5555");
        homePage.editComputer("z5555");
        editComputer.clickOnDelete();
        homePage.searchForComputer("z5555");
        homePage.verifyTextForHeaderIs("No computers found");
    }

    @Test
    public void updateComputer(){
        HomePage homePage = new HomePage(driver);
        EditComputer editComputer = new EditComputer(driver);

        homePage.searchForComputer("z2222");
        homePage.verifyValuesForRowAre(1,"z2222","17 Feb 1990","14 Feb 2019","IBM");
        homePage.editComputer("z2222");
        editComputer.addComputerDetails("z3333", "2000-01-01","2020-02-20","Apple Inc.");
        homePage.searchForComputer("z3333");
        homePage.verifyValuesForRowAre(1,"z3333","01 Jan 2000","20 Feb 2020","Apple Inc");
    }
}