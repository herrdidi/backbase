package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddNewComputer extends BasePage {

    private String XPATH_ERROR_FOR = "//div[contains(@class,'error')]//label[text()='%s']";

    public AddNewComputer(WebDriver driver){
        super(driver);
    }

    @FindBy(xpath = "//input[@class = 'btn primary']")
    public WebElement submitBtn;
    @FindBy(xpath = "//input[@id = 'name']")
    public WebElement computerNameInput;
    @FindBy(xpath = "//input[@id = 'introduced']")
    public WebElement introducedDateInput;
    @FindBy(xpath = "//input[@id = 'discontinued']")
    public WebElement discontinuedDateInput;
    @FindBy(xpath = "//select[@id = 'company']")
    public WebElement companyDropDown;

    public void addComputerDetails(String computerName, String introducedDate, String discontinuedDate, String companyName){
        Select companySelect = new Select(companyDropDown);
        computerNameInput.clear();
        computerNameInput.sendKeys(computerName);
        introducedDateInput.clear();
        introducedDateInput.sendKeys(introducedDate);
        discontinuedDateInput.clear();
        discontinuedDateInput.sendKeys(discontinuedDate);
        if (companyName != null) {
            companySelect.selectByVisibleText(companyName);
        }
        submitBtn.click();
    }

    public void addComputerDetails(String computerName){
        addComputerDetails(computerName,"","",null);
    }

    public void verifyErrorFor(String inputField){
        assertTrue(isDisplayed(String.format(XPATH_ERROR_FOR, inputField))
                ,"\nElement located by XPath:\"" + String.format(XPATH_ERROR_FOR,inputField)
                + " was not found :(");
    }
}