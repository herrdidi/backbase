package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EditComputer extends AddNewComputer{

    public EditComputer(WebDriver driver){
        super(driver);
    }

    @FindBy(xpath = "//input[@class = 'btn danger']")
    public WebElement deleteBtn;

    public void clickOnDelete(){
        deleteBtn.click();
    }
}
