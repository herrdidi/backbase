package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Header extends BasePage{

    public Header(WebDriver driver){
        super(driver);
    }

    @FindBy(xpath = "//header[@class = 'topbar']//a")
    public WebElement headerLink;

    public void verifyTopBarText(){
        assertEquals(headerLink.getText(), "Play sample application — Computer database");
    }
}