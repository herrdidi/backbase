package pages;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HomePage extends BasePage{

    private String XPATH_COMPUTER_NAME_ROW      = "//tbody//tr[%s]//a";
    private String XPATH_INTRODUCED_DATE_ROW    = "//tbody//tr[%s]//td[2]";
    private String XPATH_DISCONTINUED_DATE_ROW  = "//tbody//tr[%s]//td[3]";
    private String XPATH_COMPANY_NAME_ROW       = "//tbody//tr[%s]//td[4]";
    private String XPATH_COMPUTER_NAME_STRING   = "//tbody//tr//a[text()=\'%s\']";
    private String XPATH_HEADER_TEXT            = "//section/h1";

    public HomePage(WebDriver driver){
        super(driver);
    }

    @FindBy(xpath = "//a[@id='add']")
    public WebElement addButton;
    @FindBy(xpath = "//input[@id='searchbox']")
    public WebElement searchBox;
    @FindBy(xpath = "//input[@id='searchsubmit']")
    public WebElement searchButton;
    @FindBy(xpath = "//tbody//tr[1]//a")
    public WebElement computerName;
    @FindBy(xpath = "//div[@id = 'pagination']//li[contains(@class, 'next')]/a")
    public WebElement nextButton;
    @FindBy(xpath = "//div[@id = 'pagination']//li[contains(@class, 'prev')]/a")
    public WebElement prevButton;
    @FindBy(xpath = "//div[@id = 'pagination']//li[contains(@class, 'current')]/a")
    public WebElement currentLabelText;
    @FindBy(xpath = "//section[@id = 'main']/h1")
    public WebElement searchResultsText;
    @FindBy(xpath = "//div[@class='alert-message warning']")
    public WebElement computerCreatedMessage;


    public void verifyNextButtonIsDisabled(){
        assertThat(getParentElementOf(nextButton).getAttribute("class"),containsString("disabled"));
    }

    public void verifyPrevButtonIsDisabled(){
        assertThat(getParentElementOf(prevButton).getAttribute("class"),containsString("disabled"));
    }

    public void verifyNextButtonIsEnabled(){
        assertEquals("next", getParentElementOf(nextButton).getAttribute("class"));
    }

    public void verifyPrevButtonIsEnabled(){
        assertEquals("prev", getParentElementOf(prevButton).getAttribute("class"));
    }

    public void verifyTextForPaginationSearchResultsIs(String searchResults){
        assertEquals(searchResults,readElementText(currentLabelText));
    }

    public void verifyTextForHeaderIs(String searchResults){
        assertEquals(searchResults,readElementText(searchResultsText));
    }

    public void verifyComputerCreadedMessage(String computerName){
        assertTrue(readElementText(computerCreatedMessage).contains(computerName));
    }

    public void verifyValuesForRowAre(int row, String computerName, String introducedDate, String discontinuedDate, String companyName){
        assertEquals(computerName,getComputerNameForRow(row),"Computer name not blabla expected: actual:");
        assertEquals(introducedDate,getIntroducedDateForRow(row),"");
        assertEquals(discontinuedDate,getDiscontinuedDateForRow(row),"");
        assertEquals(companyName,getCompanyNameForRow(row),"");
    }

    public void verifyComputerIsPresent(String computerName){
        boolean flag = false;
        for(int i = 1; i <= 10; i++){
            if(computerName.equals(getComputerNameForRow(i)))
                flag = true;
        }
        assertTrue(flag);
    }

    public void verifyValueUnderRowForColumnIs(int row, String columnName, String value){
        switch(columnName) {
            case "Computer Name":
                assertEquals(value,getComputerNameForRow(row));
                break;
            case "Introduced":
                assertEquals(value,getIntroducedDateForRow(row));
                break;
            case "Discontinued":
                assertEquals(value,getDiscontinuedDateForRow(row));
                break;
            case "Company":
                assertEquals(value,getCompanyNameForRow(row));
                break;
        }
    }

    public void searchForComputer(String computerName){
        searchBox.sendKeys(computerName);
        searchButton.click();
    }

    public void editComputer(String computerName){
        searchBox.sendKeys(computerName);
        searchButton.click();
        clickComputerNameFor(computerName);
    }

    public void goToNextPage(){
        verifyNextButtonIsEnabled();
        nextButton.click();
    }

    public void goToPrevPage(){
        verifyPrevButtonIsEnabled();
        prevButton.click();
    }

    private void clickComputerNameFor(String name){
        findObjectByXPath(String.format(XPATH_COMPUTER_NAME_STRING,name)).click();
    }

    private String getComputerNameForRow(int row){
        return getInnerTextForXPath(String.format(XPATH_COMPUTER_NAME_ROW, row));
    }
    private String getIntroducedDateForRow(int row){
        return getInnerTextForXPath(String.format(XPATH_INTRODUCED_DATE_ROW, row));
    }
    private String getDiscontinuedDateForRow(int row){
        return getInnerTextForXPath(String.format(XPATH_DISCONTINUED_DATE_ROW, row));
    }
    private String getCompanyNameForRow(int row){
        return getInnerTextForXPath(String.format(XPATH_COMPANY_NAME_ROW, row));
    }
}
