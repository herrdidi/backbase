package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    protected WebDriver driver;
    private WebDriverWait wait;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10, 10);
        PageFactory.initElements(driver, this);
    }

    protected WebElement getParentElementOf(WebElement childElement){
        return (WebElement)((JavascriptExecutor)driver).executeScript("return arguments[0].parentNode;", childElement);
    }
    protected WebElement findObjectByXPath(String xpath){
        WebElement element = null;
        try{
            element = driver.findElement(By.xpath(xpath));
        }finally {
            return element;
        }
    }
    protected void waitForElementToAppear(By locator) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
    protected boolean isDisplayed(String xpath){
        return !driver.findElements(By.xpath(xpath)).isEmpty();
    }
    protected String readElementText(String xpath){
        return driver.findElement(By.xpath(xpath)).getText();
    }
    protected String readElementText(WebElement element){
        return element.getText();
    }
    protected String getInnerTextForXPath(String xpath){
        return getAttributeTextForXPath("innerText", xpath);
    }
    protected String getAttributeTextForXPath(String attr, String xpath){
        return driver.findElement(By.xpath(xpath)).getAttribute(attr);
    }
}
