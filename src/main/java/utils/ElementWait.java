package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.NoSuchElementException;

import java.time.Duration;

public class ElementWait {
    public void untilElementWithIdIsPresent(String elementId, WebDriver driver, int timeOut) {
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(timeOut))
                .pollingEvery(Duration.ofMillis(100))
                .ignoring(NoSuchElementException.class);
        wait.until(driver1 -> driver1.findElement(By.id(elementId)));
    }

    public void untilElementWithXpathIsPresent(String elementId, WebDriver driver, int timeOut) {
        new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(timeOut))
                .pollingEvery(Duration.ofMillis(100))
                .ignoring(NoSuchElementException.class)
                .until(driver1 -> driver1.findElement(By.xpath(elementId)));
    }
}
