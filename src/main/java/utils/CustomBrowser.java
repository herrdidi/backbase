package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.Properties;

public class CustomBrowser {
    private String browserType;
    private String url;
    private String driverPath;
    private WebDriver customDriver;
    private ChromeOptions options = new ChromeOptions();
    private Properties properties = new PropertiesReader().setProperties();

    public CustomBrowser(){
        setBrowserType();
        setURL();
        setDriverPath();
        setOptions();
    }

    public WebDriver getDriver(){
        return customDriver;
    }

    public void startBrowser(){
        if(browserType.toLowerCase().equals("chrome"))
            customDriver = new ChromeDriver(options);
        else
            System.out.println("Invalid browser");
        customDriver.get(url);
    }

    private void setOptions(){
        System.setProperty("webdriver.chrome.driver", driverPath);
        options.addArguments("start-maximized");
        options.setCapability(ChromeOptions.CAPABILITY, options);
    }
    private void setBrowserType(){
        browserType = properties.getProperty("browser");
    }
    private void setURL(){
        url = properties.getProperty("url");
    }
    private void setDriverPath(){
        if(browserType.toLowerCase().equals("chrome"))
            driverPath = properties.getProperty("chromeDriverPath");
        else
            System.out.println("invalid browser");
    }
}
