package utils;

import java.io.*;
import java.util.Properties;

public class PropertiesReader {

    private String path = "..//config.properties";
    public Properties properties = new Properties();

    public Properties setProperties() {
        final InputStream streamResource = PropertiesReader.class.getResourceAsStream(path);
        try{
            properties.load(streamResource);
        }catch(IOException e){
            throw new RuntimeException(e);
        }
        return properties;
    }

    public String getProperty(String key){
        return (String) properties.get(key);
    }
    public int getIntProperty(String key){
        return Integer.valueOf((String) properties.get(key));
    }
}
